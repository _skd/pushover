#!/bin/sh

po_token=
po_user=

die()
{
	echo "$1" 1>&2
	exit 1
}

usage()
{
	die "usage: ${0##*/} [-d] [-t title]"
}

dflag=0
while getopts dt: arg; do
	case ${arg} in
	d)	dflag=1 ;;
	t)	title=${OPTARG} ;;
	*)	usage ;;
	esac
done
shift $((OPTIND - 1))
[ "$#" -eq 0 ] || usage

if ! command -v curl >/dev/null; then
	die "${0##*/}: curl: command not found"
fi

if [ -z "${po_token}" ] || [ -z "${po_user}" ]; then
	die "${0##*/}: pushover variables not defined"
fi

zpool_status=$(zpool status -x 2>&1)

if [ "${zpool_status}" != "all pools are healthy" ] || [ "${dflag}" -eq 1 ]
then
	curl --show-error -w "\n" \
		--form-string "token=${po_token}" \
		--form-string "user=${po_user}" \
		--form-string "title=${title:-$(hostname)}" \
		--form-string "message=${zpool_status}" \
		https://api.pushover.net/1/messages.json
fi
